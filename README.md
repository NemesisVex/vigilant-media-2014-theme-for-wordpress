# Vigilant Media 2014 Theme

A custom theme for the Vigilant Media blog [Bitwise Dilettante](http://blog.vigilantmedia.com/).

Dependencies
------------

* GruntJS

Plugin suport
-------------

* Movable Type ID Mapper

## Implementations

### Bootstrap Overrides for WordPress

`TemplateTags::paging_nav()` checks whether the Bootstrap Overrides plugin is enabled. If so,
it uses `Setup::wp_page_menu_args()` to pass arguments to the overridden paging functions.
See the [plugin documentation](https://bitbucket.org/NemesisVex/bootstrap-overrides-for-wordpress)
for usage.
